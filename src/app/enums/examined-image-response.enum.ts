export enum ExaminedImageResponse {
 
MEL = 'Μελάνωμα (Melanoma)',
NV = 'Μελανοκυτταρικός σπίλος (Melanocytic nevus)',
BCC = 'Βασικοκυτταρικό καρκίνωμα (Basal cell carcinoma)',
AK = 'Ακτινική κεράτωση (Actinic keratosis)',
BKL = 'Καλοήθης κεράτωση (Benign keratosis - solar lentigo / seborrheic keratosis / lichen planus-like keratosis)',
DF = 'Δερματοϊνώματα (Dermatofibroma)',
VASC = 'Αγγειακή βλάβη (Vascular lesion)',
SCC = 'Ακανθοκυτταρικό καρκίνωμα (Squamous cell carcinoma)',
UNK = 'Δε βρέθηκε κάποια διάγνωση'
}
